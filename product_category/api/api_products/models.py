from django.db import models
from .utils import BaseModel
from treebeard.mp_tree import MP_Node


# Create your models here.
class Category(BaseModel, MP_Node):
    name = models.CharField(max_length=30)
    node_order_by = ['name']

    def __str__(self):
        return 'Category: {}'.format(self.name)


class Product(BaseModel):
    name = models.CharField(max_length=30)
    price = models.FloatField()
    category = models.ManyToManyField(
        Category)

    def __str__(self):
        return self.name
