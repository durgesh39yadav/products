from rest_framework import serializers
from .models import Category, Product

get = lambda node_id: Category.objects.get(pk=node_id)


class CategorySerializer(serializers.ModelSerializer):
    name = serializers.CharField()
    parent_id = serializers.IntegerField(required=False)
    child_category = serializers.SerializerMethodField()

    def get_child_category(self, obj):
        return CategorySerializer(many=True, instance=get(obj.pk).get_children()).data

    def create(self, validated_data):
        name = self.validated_data.get('name')
        parent_id = self.validated_data.get('parent_id', None)
        if 'parent_id' in self.validated_data and not Category.objects.filter(
                id=int(self.validated_data['parent_id'])).exists():
            raise serializers.ValidationError("parent ID does not exists!")
        if Category.objects.filter(name=self.validated_data['name']).exists():
            raise serializers.ValidationError("Category already exists!")
        if parent_id:
            obj = get(int(parent_id)).add_child(name=name)
        else:
            obj = Category.add_root(name=name)
        return obj

    def update(self, instance, validated_data):
        name = self.validated_data.get('name', instance.name)
        instance.name = name
        instance.save()
        return instance

    class Meta:
        model = Category
        fields = '__all__'
        read_only_fields = ('path', 'depth')


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'
