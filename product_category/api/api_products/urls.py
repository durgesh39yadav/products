from django.urls import path

from .views import CategoryViewSet, ProductViewSet

from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'product', ProductViewSet)
router.register(r'category', CategoryViewSet)

urlpatterns = [

              ] + router.urls
